"""brain_two URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todos.views import TodoListView, TodoDetailView
from django.views.generic.base import RedirectView
from todos.views import TodoCreateView
from todos.views import TodoUpdateView
from todos.views import TodoDeleteView
from todos.views import TodoItemCreateView
from todos.views import TodoitemUpdateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path("todos/", TodoListView.as_view(), name="list_todos"),
    path("todos/<int:pk>/edit", TodoUpdateView.as_view(), name="update_todolist"),
    path("", RedirectView.as_view(url="/todos")),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoCreateView.as_view(), name="create_todolist"),
    path("<int:pk>/delete", TodoDeleteView.as_view(), name="delete_todolist"),
    path("items/create/", TodoItemCreateView.as_view(), name="create_todoitem"),
    path("todos/items/<int:pk>/edit", TodoitemUpdateView.as_view(), name="update_todoitem")
]
