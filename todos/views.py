from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoItem, TodoList
from django.urls import reverse_lazy

# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"

class TodoCreateView(CreateView):
    model = TodoList
    template_name = "create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', args=[self.object.id])

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = 'update.html'
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "delete.html"
    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitem/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoitemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todoitem/update.html"
    fields = ["task", "Due date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])



