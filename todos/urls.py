from django.urls import path

from todos.views import TodoListView
from todos.views import TodoDetailView
from todos.views import TodoCreateView
from todos.views import TodoUpdateView
from todos.views import TodoDeleteView
from todos.views import TodoItemCreateView
from todos.views import TodoitemUpdateView

urlpatterns = [
    path("", TodoListView.as_view(), name="list_todos"),
    path("todos/", TodoListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoCreateView.as_view(), name="create_todolist"),
    path("/todos/<int:pk>/edit", TodoUpdateView.as_view(), name="update_todolist"),
    path("<int:pk>/delete/",TodoDeleteView.as_view(),name="delete_todolist"),
    path("items/create/", TodoItemCreateView.as_view(), name="create_todoitem"),
    path("todos/items/<int:pk>/edit", TodoitemUpdateView.as_view(), name="update_todoitem") 
]